"use strict";

// [Section] JavaScript Synchronous and Asynchronous

    // JavaScript is by default synchronous - means that only one statement can be executed at a time
    // Asynchronous means that we can proceed to execute other statement while time consuming codes are running in the back ground

    // The Fetch API allows you to asynchronously request for a resource (data)
    // A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

    // The "fetch" method will return a "promise" that resolves to a "response" object
    // .then method captures the "response" object and returns another promise
    /* 
        Syntax
            fetch("URL").then((response) => {})
    */

    fetch("https://jsonplaceholder.typicode.com/posts").then( (response) => {
        console.log(response.status);
    });


    // [Retrieve contents/data from the response object]

    fetch("https://jsonplaceholder.typicode.com/posts")
        .then((response)=>response.json())
        .then((json)=>{console.log(json)});

    // The "async" and "await" keywords is another approach that can be used to achieve an asynchronous code
    // used in function to indicate which portion of code should be waited for

    // [Creates an asynchronous function]

    async function fetchData() {

        // waits for the "fetch" method to complete then stores the value in the result variable
        let result = await fetch("https://jsonplaceholder.typicode.com/posts");

        // result returned by fetch is a "promise"
        console.log(`Result`);
        console.log(result);

        // the returned "response" is an object
        console.log(`Type`);
        console.log(typeof(result));

        // We cannot access the content of the "response" directly by accessing its body property
        console.log(`Body`);
        console.log(result.body);

        let json = await result.json();
        console.log(`JSON`);
        console.log(json);
    }

    fetchData();

    // [Retrieve a specific post]

    fetch("https://jsonplaceholder.typicode.com/posts/1")
        .then((response)=>response.json())
        .then((json)=>console.log(json));

    // [Create Post]
    /* 
        Syntax:
            fetch("URL", option).then((response) => {

            }).then((response) => {

            });
    */

    fetch("https://jsonplaceholder.typicode.com/posts", {
        method:"POST",
        headers: {
            "Content-type":"application/json"
        },
        body: JSON.stringify({
            title:"New Post",
            body:"Hello World",
            userId: 1
        })
    })
    .then((response)=>response.json())
    .then((json)=>console.log(json));

    // [Update a post using PUT method]

    fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method: "PUT",
        headers: {
            "Content-type" : "application/json"
        },
        body: JSON.stringify({
            id:1,
            title:"Updated post",
            body:"Hello again",
            userId:1
        })
    });

    // [Update a post using PATCH method

    // PATCH is used to update the whole object
    // PUT is  used to update a single property

    fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method: "PATCH",
        headers: {
            "Content-type" : "application/json"
        },
        body: JSON.stringify({
            id:1,
            title:"Updated post",
            body:"Hello again",
            userId:1
        })
    })
    .then((response)=>response.json())
    .then((json)=>console.log(json));
    

    // [Delete a post]
    fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method: "DELETE"
    });